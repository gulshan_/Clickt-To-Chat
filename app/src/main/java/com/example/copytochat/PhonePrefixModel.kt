package com.example.copytochat

data class PhonePrefixModel(
    val countryName: String? = null,
    val countryPrefix: String? = null
)