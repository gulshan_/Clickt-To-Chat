package com.example.copytochat

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import com.example.copytochat.databinding.CountryPrefixDialogBinding

class CountryPrefixDialog(private val listener: OnClick) : DialogFragment() {
    private lateinit var binding: CountryPrefixDialogBinding
    private var adapter: PhonePrefixAdapter? = null

    private val phonePrefixClickListener = object : PhonePrefixAdapter.ClickAction {
        override fun selectPrefix(phone: PhonePrefixModel?) {
            listener.selectPrefix(phone)
            dialog?.dismiss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DialogCustom)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = CountryPrefixDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.clearButton.setOnClickListener {
            binding.tvSearch.text?.clear()
        }
        rightDrawableVisibility("")

        binding.tvSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                rightDrawableVisibility(p0.toString())
                adapter?.filter(p0.toString())
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
        adapter = PhonePrefixAdapter(phonePrefixClickListener)

        val list = resources.getStringArray(R.array.country_data).countryPrefixList()
        adapter?.setData(list)
        binding.recycler.adapter = adapter
    }

    fun rightDrawableVisibility(text: String) {
        if (text?.isEmpty() == true) {
            binding.tvSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        } else {
            binding.tvSearch.setCompoundDrawablesWithIntrinsicBounds(
                0, 0,
                com.google.android.material.R.drawable.ic_mtrl_chip_close_circle, 0
            )
        }
    }

    interface OnClick {
        fun selectPrefix(phone: PhonePrefixModel?)
    }
}