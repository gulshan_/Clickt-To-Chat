package com.example.copytochat

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.copytochat.databinding.PhoneHistoryDialogBinding

class PhoneHistoryDialog(private val listener: OnClick) : DialogFragment() {
    private lateinit var binding: PhoneHistoryDialogBinding
    private var adapter: PhoneHistoryAdapter? = null
    private lateinit var pref: SharedPreferences
    var list: ArrayList<PhoneModel?>? = null

    private val phoneHistory =
        object : PhoneHistoryAdapter.ClickAction {
            override fun delete(phone: PhoneModel?) {
                list?.remove(phone)
                pref.edit().putString(Constants.PHONE_HISTORY, list?.saveObjects()).apply()
                adapter?.setData(list)
                changeMessageVisibility(list?.size)
            }

            override fun select(phone: String?) {
                listener.numberSelect(phone)
                dialog?.dismiss()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DialogCustom)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = PhoneHistoryDialogBinding.inflate(layoutInflater)
        pref = requireActivity().getPreferences(Context.MODE_PRIVATE)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = PhoneHistoryAdapter(phoneHistory)
        binding.clearAllall.setOnClickListener {
            pref.edit().putString(Constants.PHONE_HISTORY, null).apply()
            list = pref.getString(Constants.PHONE_HISTORY, null)?.getSavedObjects()
            adapter?.setData(list)
            binding.message.visibility = View.VISIBLE
        }
        list = pref.getString(Constants.PHONE_HISTORY, null)?.getSavedObjects()
        changeMessageVisibility(list?.size)

        adapter?.setData(list)
        binding.recycler.adapter = adapter
    }

    fun changeMessageVisibility(size: Int?) {
        if (size == 0 || size == null) {
            binding.message.visibility = View.VISIBLE
        }
    }

    interface OnClick {
        fun numberSelect(phone: String?)
    }
}


