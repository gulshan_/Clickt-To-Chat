package com.example.copytochat.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.copytochat.R

// Set of Material typography styles to start with
//20,600()
//16,400
//14,300(Light)
object MyFont {
    val fonts = FontFamily(
        Font(R.font.montserrat_regular,FontWeight.Normal),
        Font(R.font.montserrat_medium,FontWeight.Medium),
        Font(R.font.montserrat_semibold,FontWeight.SemiBold)
    )
    val Typography = Typography(
        h6 = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.SemiBold,
            fontSize = 20.sp
        ),
        body1 = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Normal,
            fontSize = 16.sp
        ),
        body2 = TextStyle(
            fontFamily = fonts,
            fontWeight = FontWeight.Light,
            fontSize = 14.sp
        )
        /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
    )
}