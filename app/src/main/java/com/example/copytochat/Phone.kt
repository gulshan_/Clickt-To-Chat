package com.example.copytochat

data class PhoneModel(
    val phone: String? = null,
    val date: String? = null
)
