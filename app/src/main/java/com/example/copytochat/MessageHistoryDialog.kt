package com.example.copytochat

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.fragment.app.DialogFragment
import com.example.copytochat.ui.theme.MyFont

class MessageHistoryDialog : DialogFragment() {
    private lateinit var pref: SharedPreferences
    var list: ArrayList<PhoneModel?>? = null
    lateinit var mutableList: MutableList<PhoneModel?>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setStyle(STYLE_NO_TITLE, R.style.DialogCustom)

        pref = requireActivity().getPreferences(Context.MODE_PRIVATE)

        list = pref.getString(Constants.MEASSAGES_HISTORY, null)?.getSavedObjects()

        return ComposeView(requireContext()).apply {
            setContent {
                mutableList = remember {
                    mutableStateListOf<PhoneModel?>().apply {
                        list?.let { addAll(it) }
                    }
                }
                val columnModifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White)
                    .padding(16.dp)
                Column(modifier = columnModifier) {
                    Title()
                    HistoryMessageList(list = mutableList.toList())
                }
            }
        }
    }

    @Composable
    fun Title() {
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = "History", style = MyFont.Typography.h6)
            Text(
                text = "Clear all",
                style = MaterialTheme.typography.body2,
                modifier = Modifier.clickable {
                    mutableList.clear()
                    pref.edit().putString(Constants.MEASSAGES_HISTORY, null).apply()
                })
        }
    }

    @Composable
    fun HistoryMessageList(list: List<PhoneModel?>?) {
        if (list?.size != 0) {
            LazyColumn(
                Modifier
                    .padding(top = 14.dp),
                verticalArrangement = Arrangement.spacedBy(8.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                itemsIndexed(mutableList.toList()) { i, item ->
                    Row(
                        Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(text = "${item?.phone}", style = MaterialTheme.typography.body1)
                        Icon(
                            painterResource(id = com.google.android.material.R.drawable.ic_mtrl_chip_close_circle),
                            contentDescription = "",
                            modifier = Modifier.clickable {
                                mutableList.remove(item)
                                pref.edit()
                                    .putString(
                                        Constants.MEASSAGES_HISTORY,
                                        ArrayList(mutableList.toList()).saveObjects()
                                    )
                                    .apply()
                            }
                        )
                    }
                }
            }
        } else {
            EmptyList()
        }
    }
}


@Composable
fun EmptyList() {
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 10.dp),
        text = "Empty list",
        style = MaterialTheme.typography.body2,
        textAlign = TextAlign.Center
    )
}


