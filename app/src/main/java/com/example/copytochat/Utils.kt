package com.example.copytochat

import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import org.json.JSONArray

fun Array<String>.isPhoneCode(number: String): Pair<String?, String?> {
    for (country in this) {
        val splitCountry = country.split(",")
        val phoneCode = splitCountry[1]
        val countryName = splitCountry[0]
        if (number.startsWith(phoneCode)) {
            return Pair(phoneCode, countryName)
        }
    }
    return Pair(null, null)
}

fun Array<String>.countryPrefixList(): ArrayList<PhonePrefixModel>? {
    var list = arrayListOf<PhonePrefixModel>()
    for (country in this) {
        val splitCountry = country.split(",")
        val phoneCode = "+${splitCountry[1]}"
        val countryName = splitCountry[0]
        list.add(PhonePrefixModel(countryName, phoneCode))
    }
    return list
}

fun TextView.linkClick() {
    this.movementMethod = LinkMovementMethod.getInstance()
}

fun Array<String>.getCountryCode(_countryName: String): String? {
    for (country in this) {
        val splitCountry = country.split(",")
        val phoneCode = splitCountry[1]
        val countryCode = splitCountry[2]
        if (countryCode.equals(_countryName, true)) {
            return phoneCode
        }
    }
    return null
}

fun String.stringToPhoneModel(): PhoneModel? {
    val regex = """PhoneModel\(phone=(.*), date=(.*)\)""".toRegex()
    val matchResult = regex.find(this)
    return matchResult?.let {
        val (phone, date) = it.destructured
        PhoneModel(phone, date)
    }
}


fun String.getSavedObjects(): ArrayList<PhoneModel?> {
    val list = arrayListOf<PhoneModel?>()
    this.let {
        val array = JSONArray(it)
        for (i in 0 until array.length()) {
            val obj = array.getString(i)
            list.add(obj.stringToPhoneModel())
        }
    }
    return list
}

fun ArrayList<PhoneModel?>.saveObjects(): String {
    val array = JSONArray()
    for (obj in this) {
        array.put(obj.toString())
    }
    return array.toString()
}

fun LinearLayout.changeVisibility(visibility: Boolean) {
    this.visibility = if (visibility) View.GONE else View.VISIBLE
}

fun CharSequence.getNumbersFromString(): String = this.replace(Regex("[^0-9]"), "")


fun String.isEmptyString(): Boolean = this == "null"

