package com.example.copytochat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PhoneHistoryAdapter(private val onItemClick: ClickAction) :
    RecyclerView.Adapter<PhoneHistoryAdapter.MyViewHolder>() {
    private val phoneList: MutableList<PhoneModel?> = arrayListOf()

    fun setData(list: List<PhoneModel?>?) {
        phoneList.clear()
        list?.let { this.phoneList.addAll(it) }
        notifyDataSetChanged()
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.phone_history_item, parent, false)
        )
    }

    override fun getItemCount(): Int = phoneList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.findViewById<TextView>(R.id.phone).text = phoneList[position]?.phone
        holder.itemView.findViewById<TextView>(R.id.date).text = phoneList[position]?.date
        holder.itemView.findViewById<ImageView>(R.id.delete).setOnClickListener {
            onItemClick.delete(phoneList[position])
        }
        holder.itemView.findViewById<LinearLayout>(R.id.number_layout).setOnClickListener {
            onItemClick.select(phoneList[position]?.phone)
        }
    }

    interface ClickAction {
        fun delete(phone: PhoneModel?)
        fun select(phone: String?)
    }

}