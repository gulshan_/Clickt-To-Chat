package com.example.copytochat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.Locale

class PhonePrefixAdapter(private val onItemClick: ClickAction) :
    RecyclerView.Adapter<PhonePrefixAdapter.MyViewHolder>() {
    private val prefixes: MutableList<PhonePrefixModel?> = arrayListOf()
    private val sortedList: MutableList<PhonePrefixModel?> = arrayListOf()

    fun setData(list: List<PhonePrefixModel?>?) {
        prefixes.clear()
        list?.let { this.prefixes.addAll(it) }
        sortedList.addAll(this.prefixes)
        notifyDataSetChanged()
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.phone_prefix_item, parent, false)
        )
    }

    override fun getItemCount(): Int = prefixes.size

    override fun onBindViewHolder(holder: MyViewHolder, i: Int) {
        holder.itemView.findViewById<TextView>(R.id.country_name).text = prefixes[i]?.countryName
        holder.itemView.findViewById<TextView>(R.id.country_code).text = prefixes[i]?.countryPrefix
        holder.itemView.findViewById<LinearLayout>(R.id.prefix_layout).setOnClickListener {
            onItemClick.selectPrefix(prefixes[i])
        }
    }

    fun filter(charText: String) {
        prefixes.clear()
        if (charText.isEmpty()) {
            prefixes.addAll(sortedList)
        } else {
            for (item in sortedList) {
                if (item?.countryName?.lowercase(Locale.getDefault())?.contains(
                        charText.lowercase(
                            Locale.getDefault()
                        )
                    ) == true || item?.countryPrefix?.contains(
                        charText
                    ) == true
                ) {
                    prefixes.add(item)
                }
            }
        }
        notifyDataSetChanged();
    }

    interface ClickAction {
        fun selectPrefix(phone: PhonePrefixModel?)
    }


}