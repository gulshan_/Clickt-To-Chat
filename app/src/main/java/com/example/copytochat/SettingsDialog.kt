package com.example.copytochat

import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.copytochat.databinding.SettingsDialogBinding

class SettingsDialog(private val listener: Clicks) : DialogFragment() {
    private lateinit var binding: SettingsDialogBinding
    private lateinit var preference: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DialogCustom)
    }
    override fun onDismiss(dialog: DialogInterface) {
        listener.onClose()
        super.onDismiss(dialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = SettingsDialogBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setListeners()
    }

    private fun setListeners() {
        with(binding) {
            preference = requireActivity().getPreferences(Context.MODE_PRIVATE)
            val deleteZeros_ = preference.getBoolean(Constants.DELETE_ZEROS, false)
            val helpVisibilities = preference.getBoolean(Constants.HELP_TEXT_VISIBILITY, false)
            val savePrefix = preference.getBoolean(Constants.SAVE_PREFIX, false)
            if (deleteZeros_) deleteZero.isChecked = true
            if (helpVisibilities) swHelpVisiblity.isChecked = true
            if (savePrefix) swSavePrefix.isChecked = true
            deleteZero.setOnCheckedChangeListener { _, isChecked ->
                preference.edit().putBoolean(Constants.DELETE_ZEROS, isChecked).apply()
            }
            swHelpVisiblity.setOnCheckedChangeListener { _, isChecked ->
                preference.edit().putBoolean(Constants.HELP_TEXT_VISIBILITY, isChecked).apply()
            }
            swSavePrefix.setOnCheckedChangeListener { _, isChecked ->
                preference.edit().putBoolean(Constants.SAVE_PREFIX, isChecked).apply()
                if (!isChecked) {
                    preference.edit().putString(Constants.SAVED_PREFIX, "").apply()
                }
            }
        }
    }

    interface Clicks {
        fun onClose()
    }
}