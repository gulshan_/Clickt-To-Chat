package com.example.copytochat

object Constants {
    const val LAST_NUMBER = "save_last_number"
    const val DELETE_ZEROS = "delete_zeros"
    const val HELP_TEXT_VISIBILITY = "help_text_visibility"
    const val SAVE_PREFIX = "save_prefix"
    const val SAVED_PREFIX = "saved_prefix"
    const val PHONE_HISTORY = "phone_history"
    const val MEASSAGES_HISTORY = "messages_history"
}