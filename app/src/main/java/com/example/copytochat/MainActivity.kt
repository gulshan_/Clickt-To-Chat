package com.example.copytochat

import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.copytochat.R.array
import com.example.copytochat.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var pref: SharedPreferences
    var phoneCode = ""
    var countryName_ = ""
    var savedPrefix: String? = ""
    var helpVisibility = false
    var savePrefix = false

    private val settingsClickListener = object : SettingsDialog.Clicks {
        override fun onClose() {
            getPrefs()
            saveSavedNumber(phoneCode)
            binding.helpLinearLayout.changeVisibility(helpVisibility)
        }
    }

    private val phoneHistoryClickListener =
        object : PhoneHistoryDialog.OnClick {
            override fun numberSelect(phone: String?) {
                binding.textInputEditText.setText(phone)
            }
        }

    private val prefixClickListener =
        object : CountryPrefixDialog.OnClick {
            override fun selectPrefix(phone: PhonePrefixModel?) {
                binding.tvCountryName.text = phone?.countryName.toString()
                binding.textInputEditText.text?.clear()
                binding.textInputEditText.setText(phone?.countryPrefix?.replace("+", ""))
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListeners()
        pref = getPreferences(Context.MODE_PRIVATE)
        getPrefs()
        with(binding) {
            textInputEditText.setText(savedPrefix)
            handleNumberFromIntent()
            val (_, countryName) = resources.getStringArray(array.country_data)
                .isPhoneCode(savedPrefix!!)
            helpLinearLayout.changeVisibility(helpVisibility)
            if (savePrefix.not()) {
                tvCountryName.text = resources.getString(R.string.country_txt)
                return
            }
            tvCountryName.text =
                if (countryName?.isEmptyString() == true) resources.getString(R.string.country_txt) else countryName
        }
    }

    private fun handleNumberFromIntent() {
        if (intent?.type == "text/plain") {
            val phoneNumber = intent?.clipData?.getItemAt(0)?.text?.getNumbersFromString()
            binding.textInputEditText.setText(phoneCode+phoneNumber)
        }
    }

    private fun getPrefs() {
        with(pref) {
            savePrefix = getBoolean(Constants.SAVE_PREFIX, false)
            savedPrefix = getString(Constants.SAVED_PREFIX, "")
            helpVisibility = getBoolean(Constants.HELP_TEXT_VISIBILITY, false)
        }
    }

    private fun setListeners() {
        with(binding) {
            txtMoreAboutApp.linkClick()
            countryCode.linkClick()
            txtCodeMore.linkClick()
            val dialog = SettingsDialog(settingsClickListener)
            imgSettings.setOnClickListener {
                if (!dialog.isAdded) {
                    dialog.show(supportFragmentManager, "")
                }
            }
            imgInfo.setOnClickListener {
                AppInfoDialog().show(supportFragmentManager, "")
            }
            clear.setOnClickListener {
                textInputEditText.text?.clear()
            }
            openWp.setOnClickListener {
                openWp(textInputEditText.text.toString(), message.text.toString())
                saveOpenedPhones()
                saveOpenedMessages()
            }

            phoneHistory.setOnClickListener {
                PhoneHistoryDialog(phoneHistoryClickListener).show(supportFragmentManager, "")
            }
            copy.setOnClickListener {
                val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                var text = clipboard.primaryClip?.getItemAt(0)?.text.toString()
                    .replace(Regex("[^0-9]"), "")

                if (text.isEmpty()) {
                    showToast(R.string.empty_clipboard)
                    return@setOnClickListener
                }

                if (!textInputEditText.text.isNullOrEmpty() || !textInputEditText.text.toString()
                        .contains(text)
                ) {
                    textInputEditText.text?.clear()
                    textInputEditText.append(text)
                }
                var s = ""
                for (i in text.toCharArray()) {
                    s += i
                    val (_, country) = resources.getStringArray(array.country_data)
                        .isPhoneCode(s)
                    if (country?.isEmptyString() == false) {
                        tvCountryName.text = country
                        return@setOnClickListener
                    }
                }
            }
            tvCountryName.setOnClickListener {
                val prefixDialog = CountryPrefixDialog(prefixClickListener)
                if (!prefixDialog.isAdded) {
                    prefixDialog.show(supportFragmentManager, "")
                }
            }

            textInputEditText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(txt: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (!txt.isNullOrEmpty()) {
                        binding.openWp.alpha = 1f
                        binding.openWp.isClickable = true

                        val text = txt.toString()
                        val (code, countryName) = resources.getStringArray(array.country_data)
                            .isPhoneCode(text)
                        countryName_ = countryName.toString()

                        if (!countryName_.isEmptyString()) {
                            tvCountryName.text = countryName_
                            phoneCode = code.toString()
                            saveSavedNumber(text)
                        } else if (phoneCode.isEmpty()) {
                            tvCountryName.text = resources.getString(R.string.country_txt)
                        }
                    } else {
//                        binding.openWp.isEnabled = false
                        binding.openWp.isClickable = false
                        binding.openWp.alpha = 0.5f
                        textInputEditText.text?.clear()
                        tvCountryName.text = resources.getString(R.string.country_txt)
                    }
                }

                override fun afterTextChanged(p0: Editable?) {}
            })

            openMessageLt.setOnClickListener {
                openMessageLt.rotation = 180f

                if (ltMessage.isVisible) {
                    ltMessage.changeVisibility(true)
                } else {
                    ltMessage.changeVisibility(false)
                }
            }
            openMessageHistory.setOnClickListener {
                MessageHistoryDialog().show(supportFragmentManager, "")
            }
        }
    }


    private fun saveSavedNumber(phone: String?) {
        if (savePrefix) {
            pref.edit().putString(Constants.SAVED_PREFIX, phone).apply()
        }
    }

    private fun openWp(number: String, text: String?) {
        var phoneNumber = number
        if (number.length == phoneCode.length) {
            showToast(R.string.empty_phone)
            return
        } else if (number.length < phoneCode.length) {
            showToast(R.string.wrong_number)
            return
        }
        if (number.toCharArray()[phoneCode.length].toString() == ZERO) {
            phoneNumber = number.replaceFirst(ZERO, "")
        }
        val url = "$WHATSAPP$phoneNumber?text=$text"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }


    private fun saveOpenedPhones() {
        var list = arrayListOf<PhoneModel?>()
        if (pref.getString(Constants.PHONE_HISTORY, "").equals("")) {
            list = arrayListOf()
        } else {
            list.addAll(pref.getString(Constants.PHONE_HISTORY, "")?.getSavedObjects()!!)
        }

        val phone = binding.textInputEditText.text.toString()
        val date = android.text.format.DateFormat.format(DATA_FORMAT, Date()).toString()
        list.add(PhoneModel(phone, date))
        pref.edit().putString(Constants.PHONE_HISTORY, list.saveObjects()).apply()
    }

    private fun saveOpenedMessages() {
        var list = arrayListOf<PhoneModel?>()
        if (pref.getString(Constants.MEASSAGES_HISTORY, "").equals("")) {
            list = arrayListOf()
        } else {
            list.addAll(pref.getString(Constants.MEASSAGES_HISTORY, "")?.getSavedObjects()!!)
        }

        val phone = binding.message.text.toString()
        val date = null
        list.add(PhoneModel(phone, date))
        pref.edit().putString(Constants.MEASSAGES_HISTORY, list.saveObjects()).apply()
    }


    private fun showToast(textId: Int) {
        Toast.makeText(
            this@MainActivity,
            getString(textId),
            Toast.LENGTH_SHORT
        )
            .show()
    }

    companion object {
        const val ZERO = "0"
        const val WHATSAPP = "https://wa.me/"
        const val DATA_FORMAT = "dd MMM."
    }
}